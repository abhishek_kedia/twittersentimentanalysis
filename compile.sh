#!/bin/sh
mkdir out
javac -d out src/twitterSentiment/model/NaiveBayesModel.java
javac -d out -cp out src/twitterSentiment/trainer/*
javac -d out -cp out src/twitterSentiment/predictor/*
chmod u+x run.sh
