package twitterSentiment.predictor;

/**
 * Created by akedia on 25/02/16.
 */
public class Prediction {
    public String tweet;
    public double positiveProbability;
    public double negativeProbability;
    public boolean sentiment;

    public Prediction(String tweet, double positiveScore, double negativeScore) {
        this.tweet = tweet;
        this.sentiment = positiveScore >= negativeScore;
        double actualPositiveScore = Math.exp(positiveScore);
        double actualNegativeScore = Math.exp(negativeScore);
        double totalScore = actualPositiveScore + actualNegativeScore;
        this.positiveProbability = actualPositiveScore/totalScore;
        this.negativeProbability = actualNegativeScore/totalScore;
//        System.out.println(String.format("%s-> +s:%s, -s:%s, sentiment:%b",tweet,positiveScore,negativeScore,sentiment)); //db
    }

    public String toString() {
        return String.format("%d,+:%.4f,-:%.4f,'%s'",((sentiment)?4:0),positiveProbability,negativeProbability,tweet);
    }
}
