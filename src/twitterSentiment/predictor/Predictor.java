package twitterSentiment.predictor;

import twitterSentiment.model.NaiveBayesModel;
import twitterSentiment.trainer.Parser;

import java.util.Set;

/**
 * Created by akedia on 25/02/16.
 */
public class Predictor {
    private NaiveBayesModel model;
    public Predictor(NaiveBayesModel model) {
        this.model = model;
    }

    private double computePositiveScore(String tweet) {
        double score = model.getLogProbabilityPositive();
        Set<String> words = Parser.processAndExtractTweet(tweet);
        for(String word : words) {
            score = score + model.getLogProbabilityWordGivenPositive(word);
        }
        return score;
    }

    private double computeNegativeScore(String tweet) {
        double score = model.getLogProbabilityNegative();
        Set<String> words = Parser.processAndExtractTweet(tweet);
        for(String word : words) {
            score = score + model.getLogProbabilityWordGivenNegative(word);
        }
        return score;
    }

    public Prediction predictClass(String tweet) {
        double positiveScore = computePositiveScore(tweet);
        double negativeScore = computeNegativeScore(tweet);
        return new Prediction(tweet,positiveScore,negativeScore);
    }

}
