package twitterSentiment.predictor;

import twitterSentiment.model.NaiveBayesModel;

import java.io.*;

/**
 * Created by akedia on 25/02/16.
 */
public class Main {

    public static NaiveBayesModel loadModelFromFile(String modeFileName) throws Exception{
        FileInputStream modelFile = new FileInputStream(modeFileName);
        ObjectInputStream ois = new ObjectInputStream(modelFile);
        NaiveBayesModel model = (NaiveBayesModel) ois.readObject();
        ois.close();
        modelFile.close();
        return model;
    }

    private static String getNextTweet(BufferedReader in) {
        String tweet = null;
        try {
            tweet = in.readLine();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tweet;
    }

    public static void predictAndPrint(Predictor predictor, BufferedReader in, PrintWriter out) {
        String tweet = getNextTweet(in);
        while(tweet != null) {
            Prediction prediction = predictor.predictClass(tweet);
//            System.out.println(prediction); //db
            out.println( (prediction.sentiment)?"4":"0" );
            tweet = getNextTweet(in);
        }
    }

    public static void main(String[] args) {
        String modelFile = "assets/model";
        String inputFile = "input.txt";
        String outputFile = "output.txt";
        if(args.length >= 1)
            inputFile = args[0];
        if(args.length >= 2)
            outputFile = args[1];
        if(args.length >= 3)
            modelFile = args[2];

        try {
            BufferedReader in = new BufferedReader(new FileReader(inputFile));
            PrintWriter out = new PrintWriter(new FileWriter(outputFile));
            Predictor predictor = new Predictor(loadModelFromFile(modelFile));
            predictAndPrint(predictor,in,out);
            in.close();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
