package twitterSentiment.trainer;

import twitterSentiment.model.NaiveBayesModel;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * Created by akedia on 25/02/16.
 */
public class Main {
    public static void testParser(String inputFile) {
        try {
            NaiveBayesModel model = Parser.parseInput(inputFile);
            System.out.println(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void generateAndSaveModel(String inputFileName, String outputFileName) throws IOException{
        NaiveBayesModel model = Parser.parseInput(inputFileName);
        FileOutputStream outFile = new FileOutputStream(outputFileName);
        ObjectOutputStream oos = new ObjectOutputStream(outFile);
        oos.writeObject(model);
        oos.close();
        outFile.close();
    }

    public static void main(String[] args) {
//        System.out.println("Testing parser..");
//        testParser("training.csv");
        String inputFile = "training.csv";
        String outputFile = "assets/model";
        System.out.println("Generating and saving model");
        try {
            generateAndSaveModel(inputFile,outputFile);
            System.out.println("Done!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
