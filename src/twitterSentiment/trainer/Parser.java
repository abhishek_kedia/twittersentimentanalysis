package twitterSentiment.trainer;

import twitterSentiment.model.NaiveBayesModel;

import java.util.*;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

class DbEntry {
    public boolean sentiment; //false for -ve and true for +ve
    public String tweet;
    DbEntry (String inputLine) {
        inputLine = inputLine.replaceAll("[^\\p{ASCII}]", ""); //remove non-ascii characters
        Pattern lineReg = Pattern.compile("^\"(\\d)\",\"(.*)\"$");
        Matcher matcher = lineReg.matcher(inputLine);
        if(matcher.find()) {
            this.sentiment = matcher.group(1).equals("4");
            this.tweet = matcher.group(2);
        }
    }
}

public class Parser {

    private static boolean isNegationWord(String word) {
        return word.matches("^.*n't$|^.*nt$|^not$|^no$|^never$");
    }

    private static String[] negateNegative(String[] continuousWords) {
        int negativeIndex = continuousWords.length;
        for(int i = 0; i < continuousWords.length; i++) {
            if( isNegationWord(continuousWords[i]) ) {
                negativeIndex = i;
                break;
            }
        }
        for(int i = negativeIndex+1; i < continuousWords.length; i++) {
            continuousWords[i] = "NOT_" + continuousWords[i];
        }
        return continuousWords;
    }

    public static Set<String> processAndExtractTweet(String tweet) {
        Set<String> distinctWords = new HashSet<>();
        String[] splitAtPunctuation = tweet.split("[,|;|.|?|!]");
        for (String continuousBlock: splitAtPunctuation) {
            String[] words = continuousBlock.split("\\s");
//            System.out.println(String.format("----> continuous block : %s\nwords : %s",continuousBlock,Arrays.toString(words))); //db
            String[] processedWords = negateNegative(words);
            distinctWords.addAll(Arrays.asList(processedWords));
        }
        distinctWords.remove(""); //remove empty strings
        return distinctWords;
    }

    private static void incrementInMap(Map<String,Integer> map, String key, int increment) {
        int currentValue = 0;
        Integer keyValue = map.get(key);
        if(keyValue != null)
            currentValue = keyValue.intValue();
        map.put(key,currentValue + increment);
    }

    //TODO : idea check by converting all words to lower case
    public static NaiveBayesModel parseInput(String inputFileName) throws IOException{
        int numPositive = 0;
        int numNegative = 0;
        Set<String> vocabulary = new HashSet<>();
        HashMap<String,Integer> wordCountPositive = new HashMap<>();
        HashMap<String,Integer> wordCountNegative = new HashMap<>();

        BufferedReader in = new BufferedReader(new FileReader(inputFileName));

        String inputLine = in.readLine();
        while(inputLine != null) {
//            System.out.println("--> Now processing line : " + inputLine); //db
            DbEntry entry = new DbEntry(inputLine);
            Set<String> words = processAndExtractTweet(entry.tweet);
//            System.out.println("Words : " + words); //db
            int positiveClassIncrement = 0;
            int negativeClassIncrement = 0;
            if(entry.sentiment) {
                numPositive++;
                positiveClassIncrement = 1;
            }
            else {
                numNegative++;
                negativeClassIncrement = 1;
            }
            for(String word : words) {
                vocabulary.add(word);
                incrementInMap(wordCountPositive,word,positiveClassIncrement);
                incrementInMap(wordCountNegative,word,negativeClassIncrement);
            }
            inputLine = in.readLine();
        }
        return new NaiveBayesModel(numPositive,numNegative,vocabulary,wordCountPositive,wordCountNegative);
    }
}
