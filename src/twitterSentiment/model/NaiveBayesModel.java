package twitterSentiment.model;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

/**
 * Created by akedia on 25/02/16.
 */
public class NaiveBayesModel implements Serializable{
    private int numPositive;
    private int numNegative;
    private Set<String> vocabulary;
    private Map<String,Integer> wordCountPositive;
    private int sumPositiveWordCount;
    private Map<String,Integer> wordCountNegative;
    private int sumNegativeWordCount;

    public NaiveBayesModel(int numPositive, int numNegative, Set<String> vocabulary, Map<String,Integer> wordCountPositive, int sumPositiveWordCount, Map<String,Integer> wordCountNegative, int sumNegativeWordCount) {
        this.numPositive = numPositive;
        this.numNegative = numNegative;
        this.vocabulary = vocabulary;
        this.wordCountPositive = wordCountPositive;
        this.sumPositiveWordCount = sumPositiveWordCount;
        this.wordCountNegative = wordCountNegative;
        this.sumNegativeWordCount = sumNegativeWordCount;
    }

    private int computeValueSum(Map<String,Integer> map) {
        int sum = 0;
        for (String word : map.keySet()) {
            sum = sum + map.get(word);
        }
        return sum;
    }

    public NaiveBayesModel(int numPositive, int numNegative, Set<String> vocabulary, Map<String,Integer> wordCountPositive, Map<String,Integer> wordCountNegative) {
        this(numPositive,numNegative,vocabulary,wordCountPositive,0,wordCountNegative,0);
        this.sumPositiveWordCount = computeValueSum(wordCountPositive);
        this.sumNegativeWordCount = computeValueSum(wordCountNegative);
    }

    public String toString() {
        int total = numPositive + numNegative;
        return String.format("Naive Baiyes Model:START{\n" +
                "   Total number of docs   = %d\n" +
                "   number of + class docs = %d\n" +
                "   number of - class docs = %d\n" +
                "               vocabulary = %s\n" +
                "   word counts in + class = %s\n" +
                "   sum of word count in + = %d\n" +
                "   word counts in - class = %s\n" +
                "   sum of word count in - = %d" +
                "}END",total,numPositive,numNegative,vocabulary,wordCountPositive,sumPositiveWordCount,wordCountNegative,sumNegativeWordCount);
    }

    public double getLogProbabilityPositive() {
        double total = numPositive + numNegative;
        return Math.log(numPositive/total);
    }

    public double getLogProbabilityNegative() {
        double total = numPositive + numNegative;
        return Math.log(numNegative/total);
    }

    public double getLogProbabilityWordGivenPositive(String word) {
        if(!vocabulary.contains(word))
            return 0;
        int count = wordCountPositive.get(word);
        double probNumerator = count + 1;
        double probDenominator = sumPositiveWordCount + vocabulary.size();
        return Math.log(probNumerator/probDenominator);
    }

    public double getLogProbabilityWordGivenNegative(String word) {
        if(!vocabulary.contains(word))
            return 0;
        int count = wordCountNegative.get(word);
        double probNumerator = count + 1;
        double probDenominator = sumNegativeWordCount + vocabulary.size();
        return Math.log(probNumerator/probDenominator);
    }

}
